package com.SE.testscripts;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.SE.CommonMethods.SeleniumEasyMehods;
import com.SE.Excelutilities.SEutilities;
import com.SE.Weblocators.SEweblocators;


//import com.SRP.CommonMethods.SRPMethods;
//import com.SRP.Excelutilities.Srpexcelutilites;

public class SEtest {
	
	
		WebDriver driver;
		@BeforeTest
		  public void beforeTest() throws IOException{  	
		
	    	File dir1 = new File("TC1_Screenshots");  //Specify the Folder name here
			dir1.mkdir( );  
		System.setProperty("webdriver.chrome.driver", "C:\\sushma\\chromedriver2_win32\\chromedriver.exe");	  
		driver = new ChromeDriver();
		      driver.manage().window().maximize();
		      System.out.println("Browser initialized");
		      
		      driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		      SeleniumEasyMehods.captureScreenShot(driver, "Step1_Browser_initialization","TC1_Screenshots");
		     
		} 
		

@Test(priority=1)
public void openwebpage() {
	try {
		driver.get(SEutilities.getURL("C:\\workspace\\SELENIUMEASY-MAVENPROJECT\\Testdata\\Testdata.xlsx", 1, 0, "URL"));
		System.out.println("SE webpage should open");
	SeleniumEasyMehods.captureScreenShot(driver, "step2_SE WEBPAGESHOULD OPEN", "TC1_Screenshots");
	SEutilities.writeresult("C:\\workspace\\SELENIUMEASY-MAVENPROJECT\\Testdata\\Results.xlsx", 1,0,1 , "SE webpage should open");
	}
	catch(Exception e) {
		e.printStackTrace();
		System.out.println("Exception undergone");
	}
}
@Test(priority=2)
public void singlecheckbox() throws IOException{
	driver.findElement(By.xpath(SEweblocators.checkbox_xpath)).click();
	System.out.println("checkbox got selected");
	SeleniumEasyMehods.captureScreenShot(driver, "checkbox got selected", "TC2_Screenshots");
}
@Test(priority=3)
public void Multiplecheckbox() throws IOException{
	driver.findElement(By.xpath(SEweblocators.checkall_xpath)).click();
	System.out.println("selected all checkboxes and Button will change to Uncheck all");
	SeleniumEasyMehods.captureScreenShot(driver, "All checkboxes got selected and Button will change to Uncheck all", "TC3_Screenshots");
	SEutilities.writeresult("C:\\workspace\\SELENIUMEASY-MAVENPROJECT\\Testdata\\Results.xlsxlePath", 2,0,1 , "All checkboxes got selected and Button will change to Uncheck all");
	
	driver.findElement(By.xpath(SEweblocators.uncheckall_xpath)).click();
	driver.findElement(By.xpath("//input[@id='check1']")).click();
	//System.out.println("uncheck all boxes got selected");
	//SeleniumEasyMehods.captureScreenShot(driver, "uncheckallboxes button got selected", "TC4_Screenshots");
	
	}
@Test(priority=4)
public void singlebutton() throws IOException{
	driver.findElement(By.xpath(SEweblocators.checkbox1_xpath)).click();
	driver.findElement(By.xpath(SEweblocators.checkbox2_xpath)).click();
	driver.findElement(By.xpath(SEweblocators.checkbox3_xpath)).click();
	driver.findElement(By.xpath(SEweblocators.checkbox4_xpath)).click();
	//driver.findElement(By.xpath(SEweblocators.checkall_xpath)).click();
}

@AfterTest()  
public void afterTest() throws IOException 
{
	
	driver.close();
  
}
}
	


	




